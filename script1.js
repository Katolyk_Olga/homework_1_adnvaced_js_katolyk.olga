// ## Теоретичні питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Будь-який об'єкт в Javascript має власний прототип prototype (колекція властивостей і методів). 
// І будь-який об'єкт може мати _proto (посилання) на інший об'єкт, який також стане його прототипом.
// Коли ми викликаємо методи об'єкту чи стукуаємось до властивості, Javascript спочатку пошукає метод чи властивість на поверхні,
// у самому об'єкті, і якщо не знайде, то піде вглиб шукати по прототипу. 
// І при наявності розширення нашого об'єкту за рахунок іншого об'єкту, Javascript буде шукати і там також.
// Сформується ланцюжок прототипів

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Виклик super() у конструкторі класу-нащадка (підкласу) викликає в першу чергу конструктор батьківського класа (суперкласа), 
// і це забезпечує коректний ланцюжок прототипів для наслідування від суперкласу.
// super() передає аргументи від підкласу в конструктор суперкласу. А нащадок успадковує властивості та методи суперкласу,
// без порушення власної ініціалізації.


// ## Завдання
// + 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// + 2. Створіть гетери та сеттери для цих властивостей.
// + 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// + 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// + 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Література:
// - [Класи на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes)
// - [Класи в ECMAScript 6](https://frontender.info/es6-classes-final/)

class Employee {
    // додам в конструктор дефолтні значення для аргументів, щоб можна було створювати пустий екземпляр,
    //  і заповнювати його потім за допомогою геттерів, сеттерів
    constructor (name = "Name", age = 0, salary = 0) {
        this._name = name,
        this._age = age,
        this._salary = salary
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }

    getEmployee() {
        return `We are hiring ${this.name}, ${this.age} years old.`
    }
}

// let employeeTest = new Employee ();
let employeeTest = new Employee ("Anna", 28, 2500);
console.log(employeeTest);
console.log(employeeTest.getEmployee());

console.log(employeeTest.name); // визиваємо геттер name, працює з екзмепляром

class Programmer extends Employee {
    constructor (name, age, salary, lang = "en") {
        super (name, age, salary);
        this.lang = lang;
    }

    get salary() {
        // return this._salary*3;
        return `Hello, ${this.name}. Your basic salary is ${this._salary}\$. We offer triple salary, it will be ${this._salary*3}\$\!`;
    }
}

// let programmerTest = new Programmer();
let programmerTest = new Programmer("Petro", 35, 3400);
 // спроба подивитись, чи наслідується публічний метод getEmployee з class Employee - наслідується :)
console.log(programmerTest.getEmployee());

console.log(programmerTest.lang); // спроба дістатись до значення властивості lang без геттера - дістає :)
console.log(programmerTest.age); // перевіряю, чи наслідуються методи get та set - наслідуються :)
console.log(programmerTest._age); // перевіряю, чи наслідуються публічні властивості в обхід get та set - наслідуються :)

console.log(programmerTest.salary); // переписуємо (оновлюємо) геттер зарплатні


let newProgrammer1 = new Programmer ("Oleg", 47, 5200);
console.log(newProgrammer1.getEmployee());
console.log(newProgrammer1.salary);


let newProgrammer2 = new Programmer ("Katya", 24, 1800);
console.log(newProgrammer2.getEmployee());
console.log(newProgrammer2.salary);